/**
 * 
 */

LoginForm = React.createClass({

	/** Component Properties **/

	propTypes: {
		onLogin: React.PropTypes.func,
		continue: React.PropTypes.string, // first preference is to call the onLogin callback, but if cannot (ie saml redirect based authentication) then just redirect to continue
		
		defaultMode: React.PropTypes.oneOf(['login','signup']),
		onModeChange: React.PropTypes.func,

		samlIdp: React.PropTypes.string, // codename for the idp
		samlIdpName: React.PropTypes.string, // friendly name for the idp

		defaultName: React.PropTypes.string, // a name to appear by default in the full name input
		defaultEmail: React.PropTypes.string, // a default email address to appear in the email input

		view: React.PropTypes.oneOf(['default','micro']),
	},
	getDefaultProps: function () {
		return {
			defaultMode: 'signup',

			view: 'default'
		}
	},

	/** State **/

	getInitialState: function () {
		return {
			mode: this.props.defaultMode, // or 'signup' => show a signup form instead of the email login form
			loading: false,
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin
	],

	/** Mode **/

	setMode: function (_mode) {
		this.setState({
			mode: _mode,
			resetSent: false,
			loading: false,
		});

		if (this.props.onModeChange) this.props.onModeChange(_mode);
	},

	/** React **/

	componentDidMount: function () {
		if (this.props.view === 'micro') {
			if (this.state.mode === 'login') {
				if (this.refs.email) this.refs.email.focus();
			} else if (this.state.mode === 'signup') {
				if (this.refs.name) this.refs.name.focus();
			}
		} else {
			if (this.refs.email) this.refs.email.focus();
		}
	},
	componentDidUpdate: function(prevProps, prevState) {
		if (prevState.mode !== this.state.mode) {
			if (this.refs.email) this.refs.email.focus();
		}
	},

	/** Render **/

	/*

	<Button disabled={this.state.loading}><FontAwesome icon="twitter-square" /> Sign Up With Twitter</Button>
	<Button disabled={this.state.loading}><FontAwesome icon="google-plus-square" /> Sign Up With Google</Button>

	*/

	render: function () {
		var classes = (this.props.className ? this.props.className + ' ' : '') + "loginForm";

		if (this.state.mode === 'login') {

			if (this.props.view === 'micro') {

				return (
					<div className={this.props.className} style={this.props.style}>
						<EqualBlocks perRow={3} widths={['40%', '40%', '20%']} valign="middle">
							<TextInput ref="email" placeholder="Your email" disabled={this.state.loading} onEnterUp={this.loginEmail_submit} defaultValue={this.props.defaultValue} />
							<TextInput ref="password" placeholder="Your password" password={true} disabled={this.state.loading} onEnterUp={this.loginEmail_submit} />
							<Button onClick={this.loginEmail_submit} disabled={this.state.loading}>Login</Button>
						</EqualBlocks>
						
						<div style={{marginTop: 15}}>
							<span style={{paddingRight: 10}}>or</span>
							<Button disabled={this.state.loading} inline={true} onClick={this.loginFacebook_click} size="regular"><FontAwesome icon="facebook-square" /> Login with Facebook</Button>
						</div>
					</div>
				);

			} else {
			
				return (
					<div className={classes} style={this.props.style}>
						<div className="content-bigpad">
							<h2 className="text-center" style={{marginBottom: 20}}>
								Welcome back!
							</h2>

							<a onClick={this.signupMode_click} className="block text-center sitelink" style={{marginBottom: 20}}>
								<b>Not a member yet? Sign up</b>
							</a>

							{this.render_externalServiceButtons('Login')}

							<h3 className="text-center" style={{marginBottom: 20}}>
								or login with your email
							</h3>

							<div style={{marginBottom: 20}}>
								<TextInput ref="email" placeholder="Your email address" disabled={this.state.loading} onEnterUp={this.loginEmail_submit} className="gapBottom" />
								<TextInput ref="password" placeholder="Your password" password={true} disabled={this.state.loading} onEnterUp={this.loginEmail_submit} className="gapBottom" />
								
								<Button disabled={this.state.loading} onClick={this.loginEmail_submit} size="large">Login</Button>

								<a onClick={this.forgotMode_click} className="block text-right sitelink" style={{paddingTop: 5}}>
									Forgot your password?
								</a>
							</div>
						</div>
					</div>
				);

			}

		} else if (this.state.mode === 'signup') {

			if (this.props.view === 'micro') {

				return (
					<div className={this.props.className} style={this.props.style}>
						<EqualBlocks perRow={3} widths={['40%', '40%', '20%']} valign="middle">
							<TextInput ref="name" placeholder="Your name" disabled={this.state.loading} onEnterUp={this.signupEmail_submit} defaultValue={this.props.defaultName} />
							<TextInput ref="email" placeholder="Your email" disabled={this.state.loading} onEnterUp={this.signupEmail_submit} defaultValue={this.props.defaultValue} />
							<Button onClick={this.signupEmail_submit} disabled={this.state.loading}>Sign Up</Button>
						</EqualBlocks>
						
						<div style={{marginTop: 15}}>
							<span style={{paddingRight: 10}}>or</span>
							<Button disabled={this.state.loading} inline={true} onClick={this.loginFacebook_click} size="regular"><FontAwesome icon="facebook-square" /> Sign Up with Facebook</Button>
						</div>
					</div>
				);

			} else {

				return (
					<div className={classes} style={this.props.style}>
						<div className="content-bigpad">
							<h2 className="text-center" style={{marginBottom: 20}}>
								Welcome to {HCConfig.site_name}!
							</h2>

							<a onClick={this.loginMode_click} className="block text-center sitelink" style={{marginBottom: 20}}>
								<b>Already a member? Login</b>
							</a>

							{this.render_externalServiceButtons('Sign Up')}

							<h3 className="text-center" style={{marginBottom: 20}}>
								or sign up with email
							</h3>

							<div style={{marginBottom: 20}}>
								<TextInput ref="email" placeholder="Your email address" disabled={this.state.loading} onEnterUp={this.signupEmail_submit} defaultValue={this.props.defaultValue} className="gapBottom" />
								<TextInput ref="name" placeholder="Your name" disabled={this.state.loading} onEnterUp={this.signupEmail_submit} defaultValue={this.props.defaultName} className="gapBottom" />
								<TextInput ref="password" placeholder="Create a password" password={true} disabled={this.state.loading} onEnterUp={this.signupEmail_submit} className="gapBottom" />
								<TextInput ref="confirmPassword" placeholder="Re-enter password" password={true} disabled={this.state.loading} onEnterUp={this.signupEmail_submit} className="gapBottom" />
								
								<Button disabled={this.state.loading} onClick={this.signupEmail_submit} size="large">Sign Up</Button>
							</div>
						</div>
					</div>
				);

			}

		} else if (this.state.mode === 'forgot') {

			return (
				<div className={classes} style={this.props.style}>
					{this.state.resetSent ? <div className="content-bigpad">
						<p className="text-center">
							We have sent details on how to reset your password to:
						</p>
						<p className="text-center">
							<b>{this.state.resetSent}</b>
						</p>
					</div> : <div className="content-bigpad">
						<div style={{marginBottom: 20}}>
							<TextInput ref="email" placeholder="Your email address" disabled={this.state.loading} onEnterUp={this.forgotPassword_submit} defaultValue={this.props.defaultValue} style={{marginBottom: 10}} />

							<Button disabled={this.state.loading} onClick={this.forgotPassword_submit} size="large">Reset my Password</Button>
						</div>

						<div className="block text-center">
							<a onClick={this.loginMode_click} className="sitelink">Login</a>
							<span style={{paddingLeft: 10, paddingRight: 10}}>/</span>
							<a onClick={this.signupMode_click} className="sitelink">Sign Up</a>
						</div>
						
					</div>}
				</div>
			);

		}

		return null;
	},

	render_externalServiceButtons: function (_action) {
		return (
			<div style={{marginBottom: 20}}>
				{this.props.samlIdp ? <Button disabled={this.state.loading} onClick={this.loginSaml_click} size="large"><FontAwesome icon="university" /> {_action ? _action + ' with ' : null}{this.props.samlIdpName}</Button> : null}
				<Button disabled={this.state.loading} onClick={this.loginFacebook_click} size="large"><FontAwesome icon="facebook-square" /> {_action ? _action + ' with ' : null} Facebook</Button>
			</div>
		);
	},

	/** UI Events **/

	signupMode_click: function () {
		this.setMode('signup');
	},
	loginMode_click: function () {
		this.setMode('login');
	},
	forgotMode_click: function () {
		this.setMode('forgot');
	},

	/** Forgot Password **/

	forgotPassword_submit: function () {
		var _email = this.refs.email.value();
		if (!_email) {
			alert('Please enter a valid email address');
			return;
		}

		this.setState({loading: true});
		Users.sendPasswordResetEmail(_email, function (error, result) {
			this.setState({loading: false});

			if (error) {
				if (error.reason) {
					alert(error.reason);
				} else {
					alert("There was a problem reseting your password, please try again");
				}

				return;
			}
			
			this.setState({resetSent: _email});
		}.bind(this));
	},

	/** Email Signup/Login **/

	loginEmail_submit: function () {
		var _email = this.refs.email.value();
		if (!_email) {
			alert('Please enter a valid email address');
			return;
		}	

		var _password = this.refs.password.value();
		if (!_password) {
			alert('Please enter your password');
			return;
		}

		this.setState({loading: true});
		Meteor.loginWithPassword(_email, _password, this.login_callback);
	},

	signupEmail_submit: function () {
		var _name = this.refs.name.value();
		if (!_name) {
			alert('Please enter your full name');
			return;
		}

		var _email = this.refs.email.value();
		if (!_email) {
			alert('Please enter your password');
			return;
		}

		// micro form does not require a password right away - it's more about data gathering to get their email/name
		// perhaps email them a password once they have signed up and allow them to click to change it (ie it's auto taken as a forgot password)

		var _password = this.refs.password ? this.refs.password.value() : null;
		
		// if (!_password) {
		// 	alert('Please enter your password');
		// 	return;
		// }

		if (_password) {
			var _confirmPassword = this.refs.confirmPassword.value();
			if (!_confirmPassword) {
				alert('Please confirm your password by entering it again');
				return;
			} else if (_confirmPassword !== _password) {
				alert('Your passwords do not match - please enter them again');
				return;
			}
		}
		
		this.setState({loading: true});

		Accounts.signup({
			email: _email,
			password: _password,
			name: _name
		}, this.signup_callback);
		
		// Accounts.createUser({
		// 	email: _email,
		// 	password: _password,
		// 	profile: {
		// 		name: _name
		// 	}
		// }, this.signup_callback);
	},

	/** Facebook Login **/

	loginFacebook_click: function () {
		analyticsEvent("login facebook attempt");

		this.setState({loading: true});

		Meteor.loginWithFacebook({
			requestPermissions: [
				'public_profile',
				'email',
				'user_friends',

				// These permissions require review by Facebook:
				//'user_about_me',
				//'user_education_history',
				//'user_work_history',
				//'user_interests',
				//'user_location',
				//'user_photos',

				// Extended permissions also requiring review by Facebook:
				//'publish_actions',
			]
		}, this.login_callback)
	},

	loginSaml_click: function () {
		Meteor.loginWithSaml(this.props.samlIdp, {
			continueUrl: this.props.continue
		});
	},

	/** Callbacks **/

	login_callback: function (error) {
		// due to logging in this form could be vanished away at some point, so hide if this is the case:
		if (!this.isMounted()) return;
		this.setState({loading: false});

		if (error) {
			if (error.reason) {
				alert(error.reason);
			} else {
				alert("There was a problem logging you in, please try again");
			}
		} else {
			analyticsEvent("logged in");
			if (this.props.onLogin) this.props.onLogin();
		}
	},

	signup_callback: function (error) {
		this.setState({loading: false});

		if (error) {
			alert("There was a problem with your sign up:\n\n"+error.reason);
		} else {
			analyticsEvent("signed up");

			// note this conversionEvent does not cover signing up for the first time with Facebook which is still shown as logging in
			audienceEvent('CompleteRegistration', {
				value: 0,
				content_name: 'signup',
				status: 'complete'
			});

			if (this.props.onLogin) this.props.onLogin();
		}
	},

});
